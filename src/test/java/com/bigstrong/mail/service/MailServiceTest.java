package com.bigstrong.mail.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.thymeleaf.TemplateEngine;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import org.thymeleaf.context.Context;


import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {

    @Resource
    MailService mailService;

    @Resource
    TemplateEngine templateEngine;

    @Test
    public void sendSimpleMail() {
        mailService.sendSimpleMail("893932690@qq.com" , "springboot的第一封邮件" , "hello ! 你好");

    }

    @Test
    public void sendHtmlMail() {

        String content =
                "<html>\n" +
                        "<body>\n" +
                        "<h3> hello zhuzhu ,这是一封html邮件!</h3>" +
                        "</body>\n" +
                "</html>";

        mailService.sendHtmlMail("1060529538@qq.com" , "springboot第一封html邮件" , content);
    }

    @Test
    public void testTmeplateMailTest() throws MessagingException {
        Context context = new Context();
        context.setVariable("id" , "007");

        String emailContent = templateEngine.process("emailTemplates" , context);

        mailService.sendHtmlMail("1060529538@qq.com" , "springboot第一封模版邮件" , emailContent);

    }
}