package com.bigstrong.mail.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


@Service
public class MailService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * 发信人
     */
    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;

    /**
     * 发送简单邮件
     */
    public void sendSimpleMail(String to , String subject , String content){

        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setText(content);
        simpleMailMessage.setFrom(from);
        mailSender.send(simpleMailMessage);
    }

    /**
     * 发送HTML邮件
     */
    public void sendHtmlMail(String to , String subject , String content) {

        logger.info("发送邮件开始：{},{},{}" , to , subject , content);
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message , true);
            helper.setFrom(from);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content ,true);

            mailSender.send(message);
            logger.info("发送邮件成功");
        } catch (MessagingException e) {
            //e.printStackTrace();
            logger.error("发送邮件失败" , e);
        }
    }
}
